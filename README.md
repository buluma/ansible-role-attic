# [attic](#attic)

An ansible role for installing and configuring Attic.

|GitHub|GitLab|Quality|Downloads|Version|Issues|Pull Requests|
|------|------|-------|---------|-------|------|-------------|
|[![github](https://github.com/buluma/ansible-role-attic/workflows/Ansible%20Molecule/badge.svg)](https://github.com/buluma/ansible-role-attic/actions)|[![gitlab](https://gitlab.com/buluma/ansible-role-attic/badges/master/pipeline.svg)](https://gitlab.com/buluma/ansible-role-attic)|[![quality](https://img.shields.io/ansible/quality/)](https://galaxy.ansible.com/buluma/attic)|[![downloads](https://img.shields.io/ansible/role/d/)](https://galaxy.ansible.com/buluma/attic)|[![Version](https://img.shields.io/github/release/buluma/ansible-role-attic.svg)](https://github.com/buluma/ansible-role-attic/releases/)|[![Issues](https://img.shields.io/github/issues/buluma/ansible-role-attic.svg)](https://github.com/buluma/ansible-role-attic/issues/)|[![PullRequests](https://img.shields.io/github/issues-pr-closed-raw/buluma/ansible-role-attic.svg)](https://github.com/buluma/ansible-role-attic/pulls/)|

## [Example Playbook](#example-playbook)

This example is taken from `molecule/default/converge.yml` and is tested on each push, pull request and release.
```yaml
---
- name: Converge
  hosts: all
  become: yes
  gather_facts: yes
  vars:
    pip_package: python3-pip
    # pip_executable: "{{ 'pip3' if pip_package.startswith('python3') else 'pip' }}"

  roles:
    - role: buluma.attic
```

The machine needs to be prepared. In CI this is done using `molecule/default/prepare.yml`:
```yaml
---
- name: Prepare
  hosts: all
  gather_facts: no
  become: yes

  roles:
    - role: buluma.bootstrap
    - role: buluma.core_dependencies
    - role: buluma.buildtools
    - role: buluma.pip
    - role: buluma.ca_certificates
    - role: buluma.openssl
```


## [Role Variables](#role-variables)

The default values for the variables are set in `defaults/main.yml`:
```yaml
---

attic_dependency_pkgs:
  - python3
  - python3-msgpack
  - openssl
  - libssl-dev
  - libacl1-dev
  - cython3
attic_git_repo: 'https://github.com/jborg/attic.git'
attic_version: '0.16'
attic_install_command: 'python3 setup.py install'
attic_server_install:
attic_client_install:
attic_user:
  username: 'attic'
  uid: '1010'
  pub_key: 'attic.pub'
  home: '/home/attic'
attic_backup_user:
  username: 'root'
  home: '/root'
  priv_key: 'attic'
  priv_key_passphrase: 'xkLsNWqnCjM6X7MFQupzTtFpbPLADvpDbdLzJF7KQxvkPi8Yte'
  pub_key: 'attic.pub'
  script_path: '/root/backup_script.sh'
attic_ssh_config: {}
attic_cron:
  path: ''
  job: '{{ attic_backup_user.script_path }}'
  user: '{{ attic_backup_user.username }}'
  minute: '0'
  hour: '*/8'
  day: '*'
  month: '*'
  weekday: '*'
```

## [Requirements](#requirements)

- pip packages listed in [requirements.txt](https://github.com/buluma/ansible-role-attic/blob/main/requirements.txt).

## [Status of used roles](#status-of-requirements)

The following roles are used to prepare a system. You can prepare your system in another way.

| Requirement | GitHub | GitLab |
|-------------|--------|--------|
|[buluma.bootstrap](https://galaxy.ansible.com/buluma/bootstrap)|[![Build Status GitHub](https://github.com/buluma/ansible-role-bootstrap/workflows/Ansible%20Molecule/badge.svg)](https://github.com/buluma/ansible-role-bootstrap/actions)|[![Build Status GitLab ](https://gitlab.com/buluma/ansible-role-bootstrap/badges/master/pipeline.svg)](https://gitlab.com/buluma/ansible-role-bootstrap)|
|[buluma.core_dependencies](https://galaxy.ansible.com/buluma/core_dependencies)|[![Build Status GitHub](https://github.com/buluma/ansible-role-core_dependencies/workflows/Ansible%20Molecule/badge.svg)](https://github.com/buluma/ansible-role-core_dependencies/actions)|[![Build Status GitLab ](https://gitlab.com/buluma/ansible-role-core_dependencies/badges/master/pipeline.svg)](https://gitlab.com/buluma/ansible-role-core_dependencies)|
|[buluma.pip](https://galaxy.ansible.com/buluma/pip)|[![Build Status GitHub](https://github.com/buluma/ansible-role-pip/workflows/Ansible%20Molecule/badge.svg)](https://github.com/buluma/ansible-role-pip/actions)|[![Build Status GitLab ](https://gitlab.com/buluma/ansible-role-pip/badges/master/pipeline.svg)](https://gitlab.com/buluma/ansible-role-pip)|
|[buluma.buildtools](https://galaxy.ansible.com/buluma/buildtools)|[![Build Status GitHub](https://github.com/buluma/ansible-role-buildtools/workflows/Ansible%20Molecule/badge.svg)](https://github.com/buluma/ansible-role-buildtools/actions)|[![Build Status GitLab ](https://gitlab.com/buluma/ansible-role-buildtools/badges/master/pipeline.svg)](https://gitlab.com/buluma/ansible-role-buildtools)|
|[buluma.ca_certificates](https://galaxy.ansible.com/buluma/ca_certificates)|[![Build Status GitHub](https://github.com/buluma/ansible-role-ca_certificates/workflows/Ansible%20Molecule/badge.svg)](https://github.com/buluma/ansible-role-ca_certificates/actions)|[![Build Status GitLab ](https://gitlab.com/buluma/ansible-role-ca_certificates/badges/master/pipeline.svg)](https://gitlab.com/buluma/ansible-role-ca_certificates)|
|[buluma.openssl](https://galaxy.ansible.com/buluma/openssl)|[![Build Status GitHub](https://github.com/buluma/ansible-role-openssl/workflows/Ansible%20Molecule/badge.svg)](https://github.com/buluma/ansible-role-openssl/actions)|[![Build Status GitLab ](https://gitlab.com/buluma/ansible-role-openssl/badges/master/pipeline.svg)](https://gitlab.com/buluma/ansible-role-openssl)|

## [Context](#context)

This role is a part of many compatible roles. Have a look at [the documentation of these roles](https://buluma.github.io/) for further information.

Here is an overview of related roles:

![dependencies](https://raw.githubusercontent.com/buluma/ansible-role-attic/png/requirements.png "Dependencies")

## [Compatibility](#compatibility)

This role has been tested on these [container images](https://hub.docker.com/u/buluma):

|container|tags|
|---------|----|
|ubuntu|jammy, bionic|

The minimum version of Ansible required is 2.1, tests have been done to:

- The previous version.
- The current version.
- The development version.



If you find issues, please register them in [GitHub](https://github.com/buluma/ansible-role-attic/issues)

## [Changelog](#changelog)

[Role History](https://github.com/buluma/ansible-role-attic/blob/master/CHANGELOG.md)

## [License](#license)

Apache-2.0

## [Author Information](#author-information)

[buluma](https://buluma.github.io/)
